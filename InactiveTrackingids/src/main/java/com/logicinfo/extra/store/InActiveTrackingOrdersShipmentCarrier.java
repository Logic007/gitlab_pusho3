package com.logicinfo.extra.store;

public class InActiveTrackingOrdersShipmentCarrier {

	private String code;
	private String description;

	public InActiveTrackingOrdersShipmentCarrier() {

	}

	public InActiveTrackingOrdersShipmentCarrier(String code, String description) {
		this.code = code;
		this.description = description;

	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "InActiveTrackingOrdersShipmentCarrier [code=" + code + ", description=" + description + "]";
	}

}
