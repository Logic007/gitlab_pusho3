package com.logicinfo.extra.store;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.logicinfo.extra.util.PropertiesReader;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ShipmentCancellationClient {
	static Logger LOGGER = Logger.getLogger(ShipmentCancellationClient.class);

	public static void callInactiveOrdrdelete(AwbCancellationRequest awbcancellationRequestObj, String source) {

		Gson gson = new GsonBuilder().create();
		System.setProperty("https.protocols", "TLSv1.1");
		String mailInfoString = gson.toJson(awbcancellationRequestObj);

		Client client = Client.create();

		WebResource webResource = client
				.resource(PropertiesReader.getProperty("sim.invalide.tracking.id.update.api.url"));

		
		ClientResponse response = webResource.type("application/json")
				.header("Authorization", "Basic " + PropertiesReader.getProperty("client.and.secret.id.base64.encoded"))
				.delete(ClientResponse.class, mailInfoString);

		System.out.println(response.getStatus());

		if (response.getStatus() == 200) {

			// update the processIndto Y and last update time to Y based on
			// source.
			try {
				System.out.println("inside trackingOrderListObj ");
				TrackingOrderList trackingOrderListObj = new TrackingOrderList();
				trackingOrderListObj.updateProcessIndFlagAndDate(awbcancellationRequestObj,source);
			} catch (SQLException e) {
				LOGGER.error("ShipmentCancellationClient::callInactiveOrdrdelete method SQLException" + e);
			} catch (Exception e) {
				LOGGER.error("ShipmentCancellationClient::callInactiveOrdrdelete method Exception" + e);
			}

		}

	}

}
